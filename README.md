# CitizensMenuRightClickBug

When a player is holding an item and an NPC is interacted with, the right click action will continue until the GUI is closed (eg. holding golden apple -> open NPC's gui -> apple gets consumed)

## Building

1. `mvn clean package`
2. Copy target/CitizensMenuRightClickBug{...}.jar to plugins folder
3. Join the server and info will be put in chat
