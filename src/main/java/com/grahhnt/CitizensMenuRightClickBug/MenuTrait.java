package com.grahhnt.CitizensMenuRightClickBug;

import org.bukkit.event.EventHandler;

import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.trait.Trait;

public class MenuTrait extends Trait {
	public MenuTrait() {
		super("MenuTrait");
	}

	// An example event handler. All traits will be registered automatically as
	// Bukkit Listeners.
	@EventHandler
	public void click(NPCRightClickEvent event) {
		// Handle a click on a NPC. The event has a getNPC() method.
		// Be sure to check event.getNPC() == this.getNPC() so you only handle clicks on
		// this NPC!
		if (event.getNPC() != this.getNPC())
			return;

		event.setCancelled(true);
		event.setDelayedCancellation(true);

		new TestGUI(event.getClicker());
	}

}
