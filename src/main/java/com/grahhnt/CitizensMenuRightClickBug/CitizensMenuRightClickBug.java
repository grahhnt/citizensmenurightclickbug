package com.grahhnt.CitizensMenuRightClickBug;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.trait.TraitInfo;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.hover.content.Text;

public class CitizensMenuRightClickBug extends JavaPlugin implements Listener {
	@Override
	public void onEnable() {
		CitizensAPI.getTraitFactory().registerTrait(TraitInfo.create(MenuTrait.class).withName("MenuTrait"));

		getServer().getPluginManager().registerEvents(this, this);
	}

	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event) {
		event.getPlayer().sendMessage("weird citizens menu right click bug");
		event.getPlayer().sendMessage(
				" > when holding an item & opening a menu from an npc, the server will continue to process the right click action (eg. holding a golden apple and right clicking on the npc will cause you to eat the golden apple)");
		event.getPlayer().spigot().sendMessage(new ComponentBuilder("[ create npc ]").color(ChatColor.YELLOW).bold(true)
				.event(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new Text("show command in chat")))
				.event(new ClickEvent(ClickEvent.Action.SUGGEST_COMMAND, "/npc create --trait MenuTrait coolnpc"))
				.create());
		event.getPlayer().getInventory().addItem(new ItemStack(Material.GOLDEN_APPLE, 1));
	}
}
