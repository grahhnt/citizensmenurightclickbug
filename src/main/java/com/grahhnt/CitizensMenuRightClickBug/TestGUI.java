package com.grahhnt.CitizensMenuRightClickBug;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class TestGUI {
	Inventory inv;

	public TestGUI(Player player) {
		inv = Bukkit.createInventory(player, 6 * 9, "cool gui");

		inv.setItem(0, new ItemStack(Material.GOLDEN_APPLE, 10));

		player.openInventory(inv);
	}
}
